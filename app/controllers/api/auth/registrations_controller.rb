class Api::Auth::RegistrationsController < ApplicationController

  before_action :authenticate_user, only: [:destroy]

  def create
    @user = User.new(register_params)
    if @user.save
      @token = ('Bearer ') + (jwt_session_create(@user.id))
      render status: :created, template: 'auth/auth.json.jbuilder'
    else
      render status: :unprocessable_entity, json: {errors: @user.errors.full_messages}
    end
  end

  def destroy
    current_user.destroy
    render status: :no_content, json: {}
  end

  private

  def register_params
    params.permit(:name, :email, :password)
  end
end