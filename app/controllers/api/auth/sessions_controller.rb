class Api::Auth::SessionsController < ApplicationController

  before_action :authenticate_user, only: [:validate_token]

  def create
    render status: :unprocessable_entity, json: {errors: ["Required params are not provided to continue"]} and return unless params[:email].present? && params[:password].present?
    @user = User.find_by(email: params[:email])
    if @user && @user.authenticate(params[:password])
      @token = ('Bearer ') + (jwt_session_create @user.id)
      render status: :created, template: 'auth/auth.json.jbuilder'
    else
      render status: :unprocessable_entity, json: {errors: ["The provided credentials are not correct. Please try again."]}
    end
  end

  def validate_token
    @token = request.headers[:Authorization]
    @user = current_user
    render status: :ok, template: 'auth/auth.json.jbuilder'
  end
end