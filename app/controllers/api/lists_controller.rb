class Api::ListsController < ApplicationController
  before_action :authenticate_user
  before_action :set_list, only: [:show, :update, :destroy]

  def create
    @list = current_user.lists.build(list_params)

    if @list.save
      render status: :created, template: "lists/show.json.jbuilder"
    else
      render status: :unprocessable_entity, json: {errors: @list.errors.full_messages}
    end
  end

  def index
    @lists = current_user.lists.order(id: :desc)
    render status: :ok, template: "lists/index.json.jbuilder"
  end

  def show
    render status: :ok, template: "lists/show.json.jbuilder"
  end

  def update
    if @list.update(list_params)
      render status: :ok, template: "lists/show.json.jbuilder"
    else
      render status: :unprocessable_entity, json: {errors: @list.errors.full_messages}
    end
  end

  def destroy
    @list.destroy
    render status: :no_content, json: {}
  end

  private

  def list_params
    params.permit(:name)
  end

  def set_list
    @list = current_user.lists.find(params[:id])
  end
end