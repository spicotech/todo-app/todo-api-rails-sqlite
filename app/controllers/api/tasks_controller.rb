class Api::TasksController < ApplicationController
  before_action :authenticate_user
  before_action :set_list
  before_action :set_task, only: [:show, :update, :destroy, :todo, :done]

  def create
    @task = @list.tasks.build(task_params)
    @task.user = current_user
    if @task.save
      render status: :created, template: "tasks/show.json.jbuilder"
    else
      render status: :unprocessable_entity, json: {errors: @task.errors.full_messages}
    end
  end

  def index
    @tasks = @list.tasks.order(id: :desc)
    render status: :ok, template: "tasks/index.json.jbuilder"
  end

  def show
    render status: :ok, template: "tasks/show.json.jbuilder"
  end

  def update
    if @task.update(task_params)
      render status: :ok, template: "tasks/show.json.jbuilder"
    else
      render status: :unprocessable_entity, json: {errors: @task.errors.full_messages}
    end
  end

  def destroy
    @task.destroy
    render status: :no_content, json: {}
  end

  def todo
    @task.todo!
    render status: :ok, template: "tasks/show.json.jbuilder"
  end

  def done
    @task.done!
    render status: :ok, template: "tasks/show.json.jbuilder"
  end

  private

  def set_list
    @list = current_user.lists.find(params[:list_id])
  end

  def task_params
    params.permit(:name)
  end

  def set_task
    @task = @list.tasks.find(params[:id])
  end
end