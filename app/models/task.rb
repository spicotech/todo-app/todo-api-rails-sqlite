class Task < ApplicationRecord
  belongs_to :list
  belongs_to :user

  validates :name, presence: true

  def todo!
    update(done: false)
  end

  def done!
    update(done: true)
  end
end
