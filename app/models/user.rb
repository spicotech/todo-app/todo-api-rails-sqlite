class User < ApplicationRecord
  has_secure_password

  has_many :lists, dependent: :destroy
  has_many :tasks, dependent: :destroy

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password, length: {minimum: 8, maximum: 72}, if: :password_required?

  before_validation :downcase_email!

  private

  def password_required?
    password_digest.nil? || !password.blank?
  end

  # downcase email
  def downcase_email!
    email&.downcase!
  end
end
