json.Authorization @token
json.user do
  json.partial! 'users/user.json.jbuilder', user: @user
end