set :stage, :production
set :rails_env, :production
set :deploy_to, "/home/deploy/apis/todo-api"
set :branch, "master"
# set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
# set :whenever_roles, %w{web app db}
server "todo-api.spico.tech", user: "deploy", roles: %w[web app db]