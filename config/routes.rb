Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/docs' => redirect('/swagger/dist/index.html?url=/oas.json')
  namespace :api do
    namespace :auth do
      post "sign_up", to: "registrations#create"
      delete "destroy", to: "registrations#destroys"
      post "sign_in", to: "sessions#create"
      get "validate_token", to: "sessions#validate_token"
    end
    resources :lists do
      resources :tasks do
        member do
          put :todo
          put :done
        end
      end
    end
  end
end
